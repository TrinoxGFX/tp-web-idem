<!-- Récupération de get_product depuis le dossier API -->
<?php require_once './api/get_product.php' ?>
<!-- Navbar -->
<nav class="d-flex justify-content-start mb-5" style="margin-left: 25%;">
    <!-- Balise "a" au niveau du logo pour pourvoir faire un retour sur la page principale. -->
    <a href="../index.php" class="btn btn-primary">Tous les jeux</a>

    <!-- Début de drop dropdown avec toggle avec clickout -->
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Par console
        </button>
        <!-- Dropdown avec liste des console plus le nombre de jeu affilier -->
        <!-- Avec potentiel de redirection avec filtre sur le type de console -->
        <ul class="dropdown-menu dropdown-menu-start">
            <?php
            get_filter_product();
            ?>
        </ul>
    </div>
</nav>