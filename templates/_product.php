<?php

// require_once './get_all_label_game.php';
// require_once './api/get_filter_product.php';

function product($data)
{
?>
    <?php
    $date = date_create($data['date_sortie']);
    $formateddate = date_format($date, 'd/m/Y');
    ?>

    <!-- Fiche produit dans "Voir detail" -->
    <div class="card mb-3" style="max-width: 900px">
        <div class="row g-0">
            <div class="col-md-4">
                <!-- faire la redirection dans le dossier des images pour une génération dynamique, -->
                <!-- plus un echo de image_path avec un alias img_p -->
                <img style="width: 100%;height: 100%" src="../src/games/<?php echo $data['img_p'] ?>" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <!-- Récupération dynamique des titre -->
                    <h5 class="card-title text-primary mb-2"><?php echo $data['titre'] ?></h5>
                    <!-- Récupération dynamique des platforme accepter pour le produit -->
                    <?php all_label($data['id']) ?>

                    <!-- Récupération dynamique des description -->
                    <p class="card-text"><strong>Synopsis </strong>: <?php echo $data['description'] ?></p>
                    <!-- Récupération dynamique des dates de sorite -->
                    <p class="card-text text-primary mb-5"><small class="text-muted">Date de sortie : </small><?php echo $formateddate ?></p>
                    <span>
                        <!-- faire la redirection dans le dossier des pegi pour une génération dynamique, -->
                        <!-- plus un echo de image_path pour le pegi et echo age_limite pour l'age limite d'uilisation-->
                        <img class="border border-secondary rounded" style="width: 50px;height: 50px;" src="../src/pegi/<?php echo $data['image_path'] ?>" alt=""> Ages :
                        <!-- Récupération dynamique de l'age limite -->
                        <?php echo $data['age_limite'] ?>+
                    </span>
                    <div class="d-flex justify-content-center mt-5">
                            <!-- Récupération dynamique des avis de la presse -->
                            <span class="avis_presse">⭐ Avis presse : <span class="text-primary"><?php echo $data['note_media'] ?></span>/20</span>
                            <!-- Récupération dynamique des avis utilisateur -->
                            <span class="avis_utilisateur">⭐ Avis utilisateur : <span class="text-primary"><?php echo $data['note_utilisateur'] ?></span>/20</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }

function all_product($data)
{
?>
    <?php
    // On rediffinie le formatage du prix dont le prix dviser par 100 avec une décimal de 2 chiffre apres la virgule
    $prix = number_format($data['prix'] / 100, 2);
    // On présice le rendu ou on remplace le '.' par la ',' pour la variable prix.
    $formatedprice = str_replace('.', ',', $prix);
    // On précise que si le prix est égale à 0 alors il faut afficher 'gratuit'
    $formatedprice = $data['prix'] == '0' ? 'GRATUIT' : $formatedprice . '€';

    $params = ['id' => $data['id']];
    $query = http_build_query($params);
    ?>

    <!-- Fiche produit dans "Tous les produit" -->
    <div class="card m-1" style="width: 14.5rem">
        <img src="../src/games/<?php echo $data['image_path'] ?>" class="card-img-top" style="height: 22rem;" alt="...">
        <div class="card-body">
            <h5 class="card-title text-primary"><?php echo $data['titre'] ?></h5>
            <small class="text-muted">
                <p>
                    <!-- on appelle la varible pour faire l'affichage. -->
                    <!-- Si il y a un "INT" 4560 sera formater pour donner 45,60€. -->
                    <!-- Ou sinon si "INT" est egal a 0 afficher "gratuit". -->
                    <?php echo $formatedprice ?>
                </p>
            </small>
            <!-- Lorsque on clique sur voir détails sa prend la 'id' du produit. -->
            <a href="../product.php?jeu_id=<?php echo $data['id'] ?>" class="btn btn-primary">Voir détails</a>
        </div>
    </div>
<?php }
