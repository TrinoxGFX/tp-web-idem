<!-- Récupération de la base de donnée. -->
<?php require_once './config.php' ?>

<!-- Récupération de la  structure de la page avec le header, la navbar et le footer. -->
<?php require_once './templates/_header.php' ?>
<?php require_once './templates/_navbar.php' ?>

<!-- Récupération des fonctionnalité. -->
<?php require_once './api/get_product.php' ?>
<!-- Récupération de al templates. -->
<?php require_once './templates/_product.php' ?>

<!-- <h1>Tous les produits</h1> -->

<!-- Fiche produit dans "Tous les produit" -->
<div class="d-flex flex-wrap justify-content-around" style="margin-left: 20%; margin-right: 20%;">
    <?php if (isset($_GET['id'])) { ?>
        <?php echo filter_console($_GET['id']); ?>
    <?php } else { ?>
        <?php echo get_all_product(); ?>
    <?php } ?> 
</div>

<!-- Récupération de la  structure de la page avec le footer. -->
<?php require_once './templates/_footer.php' ?>