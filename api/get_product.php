<?php

function get_product($data)
{
    // Récupération du jeu_id en le mettent en Int
    $id = intval($_GET['jeu_id']);

    // Création de la request pour les details produits
    $query = "SELECT j.id,
                     j.titre,
                     j.description,
                     j.date_sortie,
                     j.image_path as img_p,
                     j.age_id,
                     ra.label as age_limite,
                     ra.image_path,
                     n.note_media,
                     n.note_utilisateur
                from jeu as j
                inner join restriction_age as ra on j.age_id = ra.id
                inner join note as n on j.note_id = n.id
                where j.id = $id ";

    // var_dump($jeu_id);
    // var_dump($query);

    // connection a la DB
    global $connect;

    // On prépare la requete SQL
    $stmt_product = mysqli_prepare($connect, $query);

    // on execute
    mysqli_stmt_execute($stmt_product);
    // var_dump($stmt_product);

    // on recupere le(s) resultat(s)
    $product_result = mysqli_stmt_get_result($stmt_product);
    // var_dump($product_result);

    // on boucle sur les resultat
    while ($id = mysqli_fetch_assoc($product_result)) {
        product($id);
        // var_dump($data);
    }
    // echo mysqli_error($connect);

};

function get_all_product()
{
    // Connection a la db
    global $connect;

    // Création de la request
    $query = "SELECT j.*
                        from jeu as j
                        ";

    // on prépare la requete SQL
    $stmt_all_product = mysqli_prepare($connect, $query);

    // on execute
    mysqli_stmt_execute($stmt_all_product);

    // On recupere le(s)resultat(s)
    $all_product_result = mysqli_stmt_get_result($stmt_all_product);
    // var_dump($all_product_result);

    // On boucle sur les resultat
    while ($data = mysqli_fetch_assoc($all_product_result)) {
        all_product($data);
        // var_dump($data);
    }
    // mysqli_close($connect);
};

function get_filter_product()
{
    // Connection a la db
    global $connect;

    // création de la requete
    // affichera toute les platforme avec le nombre de jeu dessus
    $query = "SELECT c.id ,c.label AS name_console, COUNT(*) AS num_game
    FROM console AS c
    INNER JOIN game_console AS gc ON gc.console_id = c.id
    INNER JOIN jeu AS j ON j.id = gc.jeu_id
    GROUP BY c.id";



    // on prépare la requete SQL
    $stmt_filter_product = mysqli_prepare($connect, $query);

    // on execute
    mysqli_stmt_execute($stmt_filter_product);

    // On recupere le(s)resultat(s)
    $filter_product_result = mysqli_stmt_get_result($stmt_filter_product);
    // var_dump($all_product_result);

    while ($row_console_name = mysqli_fetch_assoc($filter_product_result)) { ?>
        <li>
            <a href="../index.php?id=<?php echo $row_console_name['id'] ?>" class="dropdown-item d-flex justify-content-start text-primary" type="button"><span style="background-color:#0d6efd, hover:white"><?php echo $row_console_name['name_console'] ?> ( <?php echo $row_console_name['num_game'] ?> )</span></a>
        </li>
    <?php  } 

    // mysqli_close($connect);
};

function all_label($data)
{
    // connection a la DB
    global $connect;

    // Création de la request pour les details produits
    $query = "SELECT console.label
    FROM jeu
    INNER JOIN game_console  ON jeu.id = game_console.jeu_id
    INNER JOIN console ON game_console.console_id = console.id
    WHERE jeu.id = $data";

    // on prépare la requete SQL
    $stmt_all_label = mysqli_prepare($connect, $query);

    // on execute
    mysqli_stmt_execute($stmt_all_label);

    // On recupere le(s)resultat(s)
    $all_label_result = mysqli_stmt_get_result($stmt_all_label);
    // var_dump($all_product_result);

    // On boucle sur les resultat
    while ($row = mysqli_fetch_assoc($all_label_result)) { ?>
        <span class="badge text-bg-primary mb-2"><?php echo $row['label'] ?></span>
<?php }

    // mysqli_close($connect);
}

function filter_console($id)
{
    // Connection a la DB
    global $connect;

    // On crée la requête 
    $id = intval($_GET['id']);
    $query = "SELECT 
          j.id, 
          j.image_path,
          j.titre,
          j.prix
          FROM jeu AS j
          INNER JOIN game_console AS gc ON j.id = gc.jeu_id
          WHERE gc.console_id = $id";

    $result = mysqli_query($connect, $query);

    while ($console_select = mysqli_fetch_assoc($result)) {
        all_product($console_select);
    }
}
