<!-- Récupération de la base de donnée. -->
<?php require_once './config.php' ?>

<!-- Récupération de la  structure de la page avec le header, la navbar et le footer. -->
<?php require_once './templates/_header.php' ?>
<?php require_once './templates/_navbar.php' ?>

<!-- Récupération des fonctionnalité. -->
<?php require_once './api/get_product.php' ?>
<!-- Récupération de al templates. -->
<?php require_once './templates/_product.php' ?>

<!-- <h2>Fiche produit</h2> -->

<!-- Fiche produit dans "get_product.php" -->
<div class="d-flex flex-wrap justify-content-center">
    <?php get_product($_GET['jeu_id']) ?>
</div>

<!-- Récupération de la  structure de la page avec le footer. -->
<?php require_once './templates/_footer.php' ?>